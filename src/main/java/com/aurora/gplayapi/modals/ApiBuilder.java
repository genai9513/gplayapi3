package com.aurora.gplayapi.modals;

import com.aurora.gplayapi.providers.DeviceInfoProvider;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Locale;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiBuilder {
    private String email;
    private String aasToken;
    private String authToken;
    private String gsfId;
    private String tokenDispenserUrl;
    private String deviceCheckInConsistencyToken;
    private String deviceConfigToken;
    private String dfeCookie;
    private Locale locale;
    private DeviceInfoProvider deviceInfoProvider;
}
