package com.aurora.gplayapi.providers;

import com.aurora.gplayapi.AndroidCheckinRequest;
import com.aurora.gplayapi.DeviceConfigurationProto;

public interface DeviceInfoProvider {
    public AndroidCheckinRequest generateAndroidCheckInRequest();

    public DeviceConfigurationProto getDeviceConfigurationProto();

    public String getUserAgentString();

    public String getAuthUserAgentString();

    public String getMccMnc();

    public int getSdkVersion();

    public int getPlayServicesVersion();
}
